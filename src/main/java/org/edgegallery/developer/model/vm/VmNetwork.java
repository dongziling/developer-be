package org.edgegallery.developer.model.vm;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class VmNetwork {

    private String networkType;

    private String descriptionZh;

    private String descriptionEn;


}
