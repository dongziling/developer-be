package org.edgegallery.developer.model.vm;

public enum VmNetworkType {
    Network_N6,
    Network_MEP,
    Network_Internet;

}
