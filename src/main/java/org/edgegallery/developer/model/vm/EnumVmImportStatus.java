package org.edgegallery.developer.model.vm;

public enum EnumVmImportStatus {
    NOTCREATE,
    CREATING,
    FAILED,
    SUCCESS
}
