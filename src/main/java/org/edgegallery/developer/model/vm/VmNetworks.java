package org.edgegallery.developer.model.vm;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class VmNetworks {

    private String name;

    private String ip;

}
