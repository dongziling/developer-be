package org.edgegallery.developer.model.vm;

public enum EnumVmCreateStatus {
    NOTCREATE,
    CREATING,
    FAILED,
    SUCCESS
}
