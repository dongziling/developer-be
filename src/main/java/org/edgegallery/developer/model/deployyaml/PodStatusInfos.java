package org.edgegallery.developer.model.deployyaml;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PodStatusInfos {

    private List<PodStatusInfo> pods;
}
