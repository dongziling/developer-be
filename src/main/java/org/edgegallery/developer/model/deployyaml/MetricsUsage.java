package org.edgegallery.developer.model.deployyaml;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MetricsUsage {
    private String cpuusage;

    private String memusage;

    private String diskusage;
}
