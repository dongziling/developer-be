package org.edgegallery.developer.model.deployyaml;

import java.util.List;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PodEventsRes {
    private List<PodEvents> pods;
}
