package org.edgegallery.developer.model.deployyaml;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValueFrom {
 private SecretKeyRef secretKeyRef;
}
