package org.edgegallery.developer.model.deployyaml;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PodContainers {
    private String containername;
    private MetricsUsage metricsusage;
}
