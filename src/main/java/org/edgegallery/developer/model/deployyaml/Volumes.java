package org.edgegallery.developer.model.deployyaml;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Volumes {
    private  String name;
    private  ConfigMap configMap;
}
