package org.edgegallery.developer.model.deployyaml;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PodEvents {

    private String podName;

    private String[] podEventsInfo;
}
